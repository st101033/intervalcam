#include <QLabel>
#include <QStandardPaths>
#include <QFileDialog>
#include <QBoxLayout>
#include <QDebug>

#include "mainwindow.h"
#include "interval_cam.h"

bool start_cam = false;
int cam_id = 0;
int interval_time = 1000;
int frame_width = 320;
int frame_height = 240;
QString directory_name;

struct image_sizes {
	int width;
	int height;
};

struct image_sizes img_sizes[6];

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
{
	/* フォント設定 */
	QWidget *widget = new QWidget;
	QString meiryo = "meiryo UI";
	widget->setFont(QFont(meiryo));
	setCentralWidget(widget);

	setWindowTitle("Interval Cam");
	setMinimumWidth(300);

	/* カメラID */
	QLabel *camid_label = new QLabel(tr("Camera ID    "));
	webcam_id = new QSpinBox;
	/* 画像サイズ */
	img_sizes[0].width = 320;
	img_sizes[0].height = 240;
	img_sizes[1].width = 640;
	img_sizes[1].height = 480;
	img_sizes[2].width = 800;
	img_sizes[2].height = 600;
	img_sizes[3].width = 960;
	img_sizes[3].height = 720;
	img_sizes[4].width = 1280;
	img_sizes[4].height = 960;
	img_sizes[5].width = 1600;
	img_sizes[5].height = 1200;
	QLabel *img_size_label = new QLabel(tr("Image size"));
	image_size = new QComboBox;
	for (int i=0;i<6;i++) {
		QString item_text = QString("%1x%2").arg(img_sizes[i].width).arg(img_sizes[i].height);
		image_size->addItem(item_text);
	}
	/* インターバル */
	QLabel *interval_label = new QLabel(tr("Interval"));
	interval_edit = new QTimeEdit;
	interval_edit->setDisplayFormat("HH:mm:ss");
	/* 保存フォルダ */
	directory_name = QDir::currentPath();
	directory_file_path = new QLineEdit;
	QLabel *directory_label = new QLabel(tr("Directory"));
	QPushButton *directory_select_button = new QPushButton(tr("Browse..."));
	connect(directory_select_button, SIGNAL(clicked()), this, SLOT(get_directory()));
	/* 撮影開始ボタン */
	start_button = new QPushButton(tr("Start"));
	connect(start_button, SIGNAL(clicked()), this, SLOT(toggle_start()));
	/* LIVE */
	live_button = new QPushButton(tr("LIVE"));
	connect(live_button, SIGNAL(clicked()), this, SLOT(toggle_live()));

	QVBoxLayout *layout = new QVBoxLayout;
	QGridLayout *layout_lb = new QGridLayout;
	QHBoxLayout *layout_db = new QHBoxLayout;
	layout_lb->addWidget(camid_label, 0, 0);
	layout_lb->addWidget(webcam_id, 0, 1);
	layout_lb->addWidget(interval_label, 1, 0);
	layout_lb->addWidget(interval_edit, 1, 1);
	layout_lb->addWidget(img_size_label, 2, 0);
	layout_lb->addWidget(image_size, 2, 1);
	layout_lb->addWidget(directory_label, 3, 0);
	layout_db->addWidget(directory_file_path);
	layout_db->addWidget(directory_select_button);
	layout_lb->addLayout(layout_db, 3, 1);
	layout->addLayout(layout_lb);
	layout->addStretch();
	layout->addWidget(start_button);
	layout->addWidget(live_button);
	widget->setLayout(layout);
}

MainWindow::~MainWindow()
{
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	destroy_window();
}

void MainWindow::get_directory()
{
	QString doc_dir = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
	/* ディレクトリ名が空の場合ドキュメントディレクトリを開く */
	if (!directory_file_path->text().isEmpty()) {
		doc_dir.clear();
		doc_dir.append(directory_file_path->text());
	}
	QString dire_name = QFileDialog::getExistingDirectory(this, tr("Open directory"), doc_dir);
	if (!dire_name.isEmpty()) {
		qDebug() << dire_name;
		directory_file_path->setText(dire_name);
		directory_name = directory_file_path->text();
	}

	return;
}

void MainWindow::toggle_start()
{
	if (start_cam) {
		start_cam = false;
		start_button->setText("Start");
		live_button->setEnabled(true);
	} else {
		QTime tmp = interval_edit->time();
		interval_time = QTime(0, 0, 0).secsTo(tmp) * 1000;
		cam_id = webcam_id->value();
		frame_width = img_sizes[image_size->currentIndex()].width;
		frame_height = img_sizes[image_size->currentIndex()].height;
		start_button->setText("Stop");
		live_button->setEnabled(false);
		start_cam = true;
	}
}

void MainWindow::toggle_live()
{
	start_button->setEnabled(false);
	live_button->setEnabled(false);
	live_button->setText("Press ESC key to close live view");

	live_cam_main(webcam_id->value(), img_sizes[image_size->currentIndex()].width, img_sizes[image_size->currentIndex()].height);

	start_button->setEnabled(true);
	live_button->setEnabled(true);
	live_button->setText("LIVE");
}

cam_thread::cam_thread(QObject* parent) : QThread(parent)
{
	connect(this, SIGNAL(finished()), this, SLOT(deleteLater()));
}

void cam_thread::run()
{
	timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(timer_cam()), Qt::DirectConnection);

	int interval_sec = 0;
	timer->setInterval(interval_sec*1000);
	timer->start();
	exec();
	timer->stop();
}

void cam_thread::timer_cam()
{
	static int id = 0;
	if (start_cam) {
		timer->setInterval(interval_time);
		interval_cam_main(cam_id, id, frame_width, frame_height, qPrintable(directory_name));
		id++;
	}
}
