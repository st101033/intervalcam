//objectTrackingTutorial.cpp

//Written by  Kyle Hounslow 2013

//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software")
//, to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
//and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
//IN THE SOFTWARE.

#include <iostream>
#include <opencv2/opencv.hpp>
#include <time.h>
#ifdef _WIN32
#include <direct.h>
#include <windows.h>
#else
#include <sys/stat.h>
#endif

#include "interval_cam.h"

//names that will appear at the top of each window
const char *windowName = "Webcam";

void destroy_window(const char *window_name);

int interval_cam_main(const int cam_id, const int image_id, const int frame_width, const int frame_height, const char *directory_name)
{
	//Matrix to store each frame of the webcam feed
	cv::Mat cameraFeed;

	//video capture object to acquire webcam feed
	cv::VideoCapture capture(cam_id);
	//open capture object at location zero (default location for webcam)
	//set height and width of capture frame
	capture.set(CV_CAP_PROP_FRAME_WIDTH, frame_width);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT, frame_height);

#ifdef _WIN32
	_mkdir(directory_name);
#else
	mkdir(directory_name, 0755);
#endif
	capture.read(cameraFeed);

	/* error handling */
	if (!capture.isOpened()) {
		cameraFeed.release();
		capture.release();
		return 1;
	}

	char filename[1025];
	sprintf(filename, "%s/%03d.png", directory_name, image_id);
	cv::imwrite(filename, cameraFeed);

	cv::imshow(windowName, cameraFeed);
	cameraFeed.release();
	capture.release();
	return 0;
}

int live_cam_main(const int cam_id, const int frame_width, const int frame_height)
{
	//Matrix to store each frame of the webcam feed
	cv::Mat cameraFeed;

	//video capture object to acquire webcam feed
	cv::VideoCapture capture(cam_id);
	//open capture object at location zero (default location for webcam)
	//set height and width of capture frame
	capture.set(CV_CAP_PROP_FRAME_WIDTH, frame_width);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT, frame_height);

	while (1) {
		capture.read(cameraFeed);

		/* error handling */
		if (!capture.isOpened()) {
			return 1;
		}

		cv::imshow(windowName, cameraFeed);
		char key = cv::waitKey(30);
		if (key == 0x1b) {
			destroy_window();
			return 0;
		}
	}
	return 0;
}

void destroy_window(const char *window_name)
{
	void *window_life = cvGetWindowHandle(window_name);
	if (window_life != NULL) {
		cv::destroyWindow(window_name);
	}
}

void destroy_window()
{
	destroy_window(windowName);
}
