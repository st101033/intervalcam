#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	cam_thread m;
	m.start();
	MainWindow w;
	w.show();

	return a.exec();
}
