#-------------------------------------------------
#
# Project created by QtCreator 2016-04-16T21:40:38
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = interval_cam
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++11 -fopenmp
CONFIG += c++11


SOURCES += src/main.cpp\
	src/mainwindow.cpp \
    src/interval_cam.cpp

HEADERS  += include/mainwindow.h \
    include/interval_cam.h

unix {
	INCLUDEPATH += ./include /usr/local/include
	DEPENDPATH += ./include /usr/local/include
	LIBS += -L/usr/local/lib \
		-fopenmp \
		-lopencv_core \
		-lopencv_highgui \
		-lopencv_imgcodecs \
		-lopencv_videoio
}

macx {
	TARGET = "intervalCam"
	LIBS += -L/usr/lib
	LIBS -= -fopenmp
	QMAKE_CXXFLAGS -= -fopenmp
}

win32 {
	INCLUDEPATH += C:\opencv\opencv-3.1.0\build_qt57\install\include include
	DEPENDPATH += C:\opencv\opencv-3.1.0\build_qt57\install\include
	CONFIG(release, debug|release) {
	LIBS += -LC:\opencv\opencv-3.1.0\build_qt57\install\x86\mingw\bin \
		-fopenmp \
		-lopencv_core310 \
		-lopencv_highgui310 \
		-lopencv_imgcodecs310 \
		-lopencv_videoio310
	}
	CONFIG(debug, debug|release) {
	LIBS += -LC:\opencv\opencv-3.1.0\build_qt57_dbg\install\x86\mingw\bin \
		-lopencv_core310d \
		-lopencv_highgui310d \
		-lopencv_imgcodecs310d \
		-lopencv_videoio310d
	}
}

RESOURCES +=

RC_FILE = src\interval_cam.rc
