#ifndef INTERVAL_CAM_H
#define INTERVAL_CAM_H

int interval_cam_main(const int cam_id, const int image_id, const int frame_width, const int frame_height, const char *directory_name);
int live_cam_main(const int cam_id, const int frame_width, const int frame_height);
void destroy_window();

#endif // INTERVAL_CAM_H

