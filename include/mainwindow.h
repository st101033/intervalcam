#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLineEdit>
#include <QPushButton>
#include <QThread>
#include <QTimer>
#include <QTimeEdit>
#include <QSpinBox>
#include <QComboBox>

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void toggle_start();
	void toggle_live();
	void get_directory();
private:
	QLineEdit *directory_file_path;
	QTimeEdit *interval_edit;
	QPushButton *start_button;
	QPushButton *live_button;
	QSpinBox *webcam_id;
	QComboBox *image_size;
protected:
	void closeEvent(QCloseEvent *event);
};

class cam_thread : public QThread
{
	Q_OBJECT

public:
	cam_thread(QObject* parent=0);

protected:
	void run();

private slots:
	void timer_cam();
private:
	QTimer *timer;
};

#endif // MAINWINDOW_H
